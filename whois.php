<?php

class whois {

 private $cache;

 function __construct() {
  require_once "whoisCache.php";
  $this->cache = new whoisCache();
 }

 //Query a WHOIS server, return plain text output
 public function query($ipaddress, $server = "whois.ripe.net") {
 
  if($this->cache->uncache($ipaddress, $server) === False) {
  
   if(strtolower($server) == "whois.ripe.net") {
    $hostArgument = "-r --no-personal"; //RIPE will ban you if you do not use these; others do not support it
   } else {
    $hostArgument = "-h ".escapeshellarg($server);
   }
  
   $out = "";
   exec("whois ".$hostArgument." ".escapeshellarg($ipaddress), $out);
   $result = implode("\n", $out);
   
   $this->cache->cache($ipaddress, $server, $result);
   return $result;
   
  } else {
  
   return $this->cache->uncache($ipaddress, $server);
  
  }
  
 }
 
 //Parse plain text WHOIS output into array of row => property => value
 private function parseOutput($whoisOutput) {
  //reference credit: https://stackoverflow.com/a/30044409/2897386
 
  $out = array(); $i = 0;
  foreach(explode("\n", $whoisOutput) as $row) {

   if(trim($row) !== "") {
    $i++;
    $colon = strpos($row, ":");
    
    if($colon === False) {
     $out[$i][] = trim($row);
    } else {
     $out[$i][strtolower(substr($row, 0, $colon))] = trim(substr($row, $colon + 1));
    }
   }
  }
  
  return $out;
 }
 
 //Parse plain text WHOIS output and search for specific property
 private function getProperty($whoisOutput, $property) {
  $out = False;
  foreach($this->parseOutput($whoisOutput) as $row) {
   if(isset($row[$property])) {
    $out = $row[$property];
    break;
   }
  }
  return $out;
 }
 
 //Query IANA WHOIS server and return value of "Refer:" property
 private function getIANARefer($ipaddress) {
  $out = $this->getProperty($this->query($ipaddress, "whois.iana.org"), "refer");
  if($out == "") {
   $out = $this->getProperty($this->query($ipaddress, "whois.iana.org"), "whois");  
  }
  return $out;
 }
 
 //"Recursive" WHOIS lookup, start with IANA refer, then ask the correct server
 public function rQuery($ipaddress) {
  return $this->query($ipaddress, $this->getIANARefer($ipaddress));
 }
 

}

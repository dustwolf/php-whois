<?php

class whoisCache {
 
 private $db;
 
 /*
  CREATE TABLE `whoisCache` (
    `server` varchar(45) NOT NULL,
    `ip` varchar(46) NOT NULL,
    `result` text,
    `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`ip`,`server`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 */

 //memory cache as well as database cache
 private $memcache = array();
 private $memchanged = array();

 function __construct() {
  require_once "database.php";
  $this->db = new database();
 }

 //cache result, log change
 public function cache($ip = "", $server = "whois.ripe.net", $result = "") {

  $this->memcache[$server][$ip] = $result;
  if(!in_array($this->memchanged, array("server" => $server, "ip" => $ip), true)) {
   $this->memchanged[] = array("server" => $server, "ip" => $ip);
  }
  
 }
 
 //commit changelog to database
 private function commit() {
  $data = array();
  if(count($this->memchanged) > 0) {
   foreach($this->memchanged as $change) {
    $data[] = "('".
                $this->db->e($change["ip"])."', '".
                $this->db->e($change["server"])."', '".
                $this->db->e($this->memcache[$change["server"]][$change["ip"]])
             ."')";
   }

   $this->db->q("
    INSERT INTO `whoisCache` (`ip`, `server`, `result`)
    VALUES ".implode(",", $data)."
        ON DUPLICATE KEY UPDATE
           `result` = VALUES(`result`),
           `timestamp` = NOW()
   ");

   $this->memchanged = array();
  }
 }
 
 public function uncache($ip = "", $server = "whois.ripe.net") {
  if(!isset($this->memcache[$server][$ip])) {
 
   $out = $this->db->flatten($this->db->q("
    SELECT `result` FROM `whoisCache`
     WHERE `ip` = '".$this->db->e($ip)."'
       AND `server` = '".$this->db->e($server)."'
   "));
   
   if(isset($out[0])) {
    $this->memcache[$server][$ip] = $out[0];
   } else {
    $this->memcache[$server][$ip] = False; //cache database misses too
   }
   
  }
  
  return $this->memcache[$server][$ip];
 }
 
 //You will want to eventually age items out of your cache because Whois records can change
 private function maintenence() {
  $this->db->q("DELETE FROM `whoisCache` WHERE `timestamp` < (NOW() - INTERVAL 1 YEAR)");
 }
 
 function __destruct() {
  $this->commit();
  $this->maintenence();
 } 

}

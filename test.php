<?php

//HTML5 output
require_once "html.php";
$document = new html();

//Load WHOIS library
require_once "whois.php";
$w = new whois();

//WHOIS query for 1.1.1.1 , return HTML output
echo nl2br($w->rQuery("1.1.1.1"));

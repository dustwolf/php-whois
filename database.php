<?php

class database {

 //Mysql connection is public if you need to do operations this class does not support
 public $link;

 //Connect on alloc
 function __construct($database = "", $user = "", $pass = "", $host = "") {
 
  require_once "database.config.php";
  $config = new databaseConfig();
  if($database == "") { $database = $config->database; }
  if($user == "") { $user = $config->username; }
  if($pass == "") { $pass = $config->password; }
  if($host == "") { $host = $config->hostname; }

  $this->link = new mysqli($host, $user, $pass, $database);
  $this->link->set_charset("utf8");

 }
 
 //MySQL query, return associative array or "success" (number of rows affected), display errors seamlessly
 function q($q, $primary = "") {
    
  $r = $this->link->query($q);
  echo $this->link->error;

  $results = array();
  if($r !== True && $r !== False) {
   while($tmp = $r->fetch_assoc()) {
    if($primary == "") {
     $results[] = $tmp;
    } else {
     $results[$tmp[$primary]] = $tmp;
    }
   }
   return $results;
  } else {
   return $this->link->affected_rows;
  }

 }
 
 //Escape parameter (saves typing)
 function e($e) {
  return $this->link->real_escape_string($e);
 }

 //Escape array of parameters, for example for passing using implode
 function ae($polje) {
  $tmp = array();
  foreach($polje as $kljuc => $vrednost) {
   $tmp[$kljuc] = $this->e($vrednost);
  }
  return $tmp;
 }

 //Parse 2D query result when querying for a single column into a 1D array
 function flatten($array = array(), $parameter = "") {
  if($array !== array()) {
   if($parameter == "") {
    reset($array);
    $tmp = $array[key($array)];
    reset($tmp);
    $parameter = key($tmp);
   }

   $out = array();
   foreach($array as $vnos) {
    $out[] = $vnos[$parameter]; 
   }
  } else {
   $out = array();
  }

  return $out;
 }

 //Seamlessly close connection on dealloc
 function __destruct() {
  $this->link->close();
 }

}

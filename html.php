<?php

class html {

 public $jQuery;
 public $chosen;
 public $bootstrap;
 public $css;
 public $font;
 public $js;

 private $jscache;

 function __construct($naslov = "", $opcije = array()) {
  $this->jQuery = isset($opcije["jQuery"]);
  $this->chosen = isset($opcije["chosen"]);
  $this->bootstrap = isset($opcije["bootstrap"]);
  if(isset($opcije["css"])) {
   $this->css = $opcije["css"];
  } else {
   $this->css = "";
  }
  if(isset($opcije["font"])) {
   $this->font = $opcije["font"];
  } else {
   $this->font = "";
  }
  if(isset($opcije["js"])) {
   $this->js = $opcije["js"];
  } else {
   $this->js = "";
  }
  if($this->chosen || $this->bootstrap) {
   $this->jQuery = True;
  }

  $this->jscache = array();
 
?>
<!DOCTYPE html>
<html>
 <head>
  <meta charset="UTF-8">
  <title><?php echo $naslov; ?></title>
  <?php if($this->jQuery) { ?>
   <script src="jquery-1.11.3.min.js"></script>
  <?php } ?>
  <?php if($this->js != "") { ?>
   <script src="<?php echo $this->js; ?>"></script>
  <?php } ?>
  <?php if($this->bootstrap) { ?>
   <link rel="stylesheet" href="bootstrap.min.css">
   <link rel="stylesheet" href="bootstrap-theme.min.css">
  <?php } ?>
  <?php if($this->chosen) { ?>
   <link rel="stylesheet" href="chosen.css">
  <?php } ?>
  <?php if($this->css != "") { ?>
   <link rel="stylesheet" href="<?php echo $this->css; ?>">
  <?php } ?>
  <?php if($this->font != "") { ?>
   <link href="<?php echo $this->font; ?>" rel="stylesheet" type="text/css">
  <?php } ?>
 </head>
 <body>
<?php
 }

 function test() {
  ?>WORKS!<?php
 }

 function addJS ($id = "default", $js = "") {
  $this->jscache[$id] = "<script>".$js."</script>";
 }

 function hereJS() {
  echo implode("", $this->jscache);
  $this->jscache = array();
 }

 function __destruct() {
?>
 <?php if($this->bootstrap) { ?>
  <script src="bootstrap.min.js"></script>
 <?php } ?>
 <?php if($this->chosen) { ?>
  <script src="chosen.jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript">
   var config = {
    '.chosen-select' : {},
    '.chosen-select-deselect' : {allow_single_deselect:true},
    '.chosen-select-no-single' : {disable_search_threshold:10},
    '.chosen-select-no-results': {no_results_text:'Ni rezultatov.'},
    '.chosen-select-width' : {width:"95%"}
   }
   for (var selector in config) {
    $(selector).chosen(config[selector]);
   }
  </script>
 <?php } ?> 
 <?php 
  if($this->jscache != array()) {
   $this->hereJS(); 
  }
 ?>
 </body>
</html>
<?php
 }

}
